{-# options_ghc -Wno-missing-deriving-strategies #-}

module MyLib where

import Data.Char (digitToInt, isDigit)
import Data.Scientific (Scientific, formatScientific)
import Data.Scientific qualified as Sci
import Data.Text qualified as T
import Path (fromAbsFile, replaceExtension)
import Path.IO (resolveFile')
import Text.Megaparsec
  ( MonadParsec (takeWhile1P, try),
    Parsec,
    Stream (chunkToTokens),
    choice,
    errorBundlePretty,
    many,
    mkPos,
    option,
    runParser,
    satisfy,
  )
import Text.Megaparsec.Char (char, eol, space1, string)
import Text.Megaparsec.Char.Lexer qualified as L
import Std hiding (many, try)

type Parser = Parsec Void Text

data Submission = Submission Text [Task] deriving (Show)
data Task = Task Text [Issue] deriving (Eq, Show)
data Issue = Issue String Scientific deriving (Eq, Show)

tasks :: [(Text, Scientific)]
tasks =
  [
    ("1", 0.1),
    ("style", 1)
  ]

main :: IO ()
main =
  do
    notesFile <-
      getArgs
        >>= \case
          [p] -> resolveFile' p
          _ -> throwString "Please provide one file path argument."
    csvFile <- replaceExtension ".csv" notesFile
    submissions <-
      (=<<) (either (throwString . errorBundlePretty) pure) $
      fmap (runParser notes $ fromAbsFile notesFile) $ -- to do
      readFileUtf8 $
      notesFile
    writeFileUtf8 csvFile (toCsv submissions)
    putTextLn "issues without points"
    putText $
      foldMap (<> "\n") $
      fmap displaySubmission $
      filterWithoutPoints $
      submissions

toCsv :: [Submission] -> Text
toCsv =
  foldMap $
    \submission@(Submission studentNr ts) ->
      "philipp," <>
      studentNr <> ",," <>
      foldMap
        (\(task, maxPoints) ->
          (\s ->
            let points = maxPoints - s
            in
              if points < 0
              then error "points < 0"
              else displayPoints points <> ("," :: Text)
          ) $
          sum $
          fmap (\(Issue _ points) -> points) $
          (\case {[issues] -> issues; [] -> []; _ -> error "duplicate task"}) $
          mapMaybe
            (\(Task t issues) ->
              if t == task then Just issues else Nothing
            ) $
          ts
        )
        tasks <>
      "\"" <>
      T.replace "\"" "\"\""
        (
          "for brevity, i simply describe what you did. there is an implicit \"unlike clearly specified by the assignment\" appended to every sentence. feel free to send me questions at phi.zander@web.de.\n\n" <>
          displaySubmission submission
        ) <>
      "\"," <>
      "\n"
      

displaySubmission :: Submission -> Text
displaySubmission (Submission _ ts) =
  foldMap
    (\(Task task issues) ->
      "- " <> task <> "\n" <>
      foldMap
        (\(Issue text points) ->
          "  - "
          <>
          toText text
          <>
          case points of
            0 -> ""
            _ -> " (-" <> displayPoints points <> ")"
          <>
          "\n"
        )
        issues
    ) $
  ts

displayPoints :: Scientific -> Text
displayPoints = toText . formatScientific Sci.Fixed Nothing

filterWithoutPoints :: [Submission] -> [Submission]
filterWithoutPoints =
  mapMaybe
    (\(Submission studentNr ts) ->
      -- Maybe Submission
      fmap (Submission studentNr) $
      -- Maybe [Task]
      guarded (not . null) $
      -- [Task]
      mapMaybe
        (\(Task task issues) ->
           -- Maybe Task
          fmap (Task task) $
          -- Maybe [Issue]
          guarded (not . null) $
          -- [Issue]
          filter (\case {Issue _ 0 -> True; _ -> False}) $
          -- [Issue]
          issues
        ) $
      -- [Task]
      ts
    )

notes :: Parser [Submission]
notes =
  many
    (Submission
      <$> (try $ string "* " *> decimal)
      <* eol
      <*> many (L.nonIndented scn $ L.indentBlock scn $ indentedList)
    ) <*
  string "* other"

indentedList :: Parser (L.IndentOpt Parser Task Issue)
indentedList =
  string "- " *> taskName <&> \task ->
  L.IndentSome
    (Just $ mkPos 3)
    (pure . Task task)
    (
      string "- "
      *>
      (
        fmap (uncurry Issue) $
        (fmap . fmap) (fromMaybe 0) $
        (someTillMaybe_
          (satisfy (!= '\n'))
          (try $
            string " (-" *>
            scientific <*
            char ')'
          )
        )
      )
    )

taskName :: Parser Text
taskName =
  choice $
  fmap string $
  sortOn (coerce T.length :: Text -> Down Int) $
  fmap fst $
  tasks

decimal :: Parser Text
decimal = takeWhile1P (Just "digit") isDigit

scn :: Parser ()
scn = L.space space1 empty empty

someTillMaybe_ :: Alternative m => m a -> m end -> m ([a], Maybe end)
someTillMaybe_ p end =
  liftA2 (\x (xs, y) -> (x : xs, y)) p (manyTillMaybe_ p end)

manyTillMaybe_ :: Alternative m => m a -> m end -> m ([a], Maybe end)
manyTillMaybe_ p end = go
  where
    go =
      (fmap ([],) $ fmap Just $ end) <|>
      liftA2 (\x (xs, y) -> (x : xs, y)) p go <|>
      pure ([], Nothing)

data SP = SP !Integer {-# UNPACK #-} !Int

scientific :: Parser Scientific
scientific = do
  c' <- option 0 L.decimal
  SP c e' <- option (SP c' 0) (try $ dotDecimal_ c')
  return (Sci.scientific c e')

dotDecimal_ :: Integer -> Parser SP
dotDecimal_ c' = do
  void (char '.')
  let mkNum = foldl' step (SP c' 0) . chunkToTokens (Proxy @Text)
      step (SP a e') c =
        SP
          (a * 10 + fromIntegral (digitToInt c))
          (e' - 1)
  mkNum <$> takeWhile1P (Just "digit") isDigit
